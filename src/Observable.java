import java.util.ArrayList;

public class Observable {
	
	private ArrayList<Observer> observerList = new ArrayList<>();
	
	public void add(Observer observer)
	{
		observerList.add(observer);
	}
	
	public void remove(Observer observer)
	{
		observerList.remove(observer);
	}
	
	public void notifyObserver()
	{
		for(Observer observer : observerList)
		{
			observer.update();
		}
	}
	
}