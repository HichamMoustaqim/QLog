
public class Producteur implements Observer{
	
	private int numProducteur;
	private Stock stock;
	
	public Producteur(int num, Stock stock)
	{
		this.numProducteur = num;
		this.stock = stock;
	}
	
    /*_______________________Methodes_______________________*/
	public void setNumProducteur(int num) {
		numProducteur = num;
	}
	
	public int getNumProducteur() {
		return numProducteur;
	}
	
	public void push(int produit) {
		
		this.stock.ajouter(produit);
	}
	
	public void clear() {
		
		this.stock.clear();
	}
	
	public void update() {
		
		int x = (int)this.stock.getTaille()*90/100;
		
		if(this.stock.getStock() >= x && this.stock.getStock() != this.stock.getTaille())
		{
			System.out.println("ATTENTION, STOCK PRESQUE PLEIN");
		}
		else if (this.stock.getStock() == this.stock.getTaille())
		{
			System.out.println("ATTENTION, STOCK PLEIN");
		}
	}

}
