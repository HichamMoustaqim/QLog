
public class Utilisateur implements Observer{
    /*_______________________Attributs_______________________*/
	private int numUtilisateur;
	private Stock stock;
	
	public Utilisateur(int num, Stock stock)
	{
		this.numUtilisateur = num;
		this.stock = stock;
	}
	
    /*_______________________Methodes_______________________*/
	public void setNumUtilisateur(int num) {
		numUtilisateur = num;
	}
	
	public int getNumUtilisateur() {
		return numUtilisateur;
	}
	
	public void pop() {
		
		this.stock.pop();
	}
	
	public void update() {
		
		int x = (int)this.stock.getTaille()*10/100;
		
		if(this.stock.getStock() <= x && this.stock.getStock() != 0)
		{
			System.out.println("ATTENTION, STOCK PRESQUE VIDE");
		}
		else if (this.stock.getStock() == 0)
		{
			System.out.println("ATTENTION, STOCK VIDE");
		}
	}
}