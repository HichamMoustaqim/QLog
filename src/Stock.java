import java.util.ArrayList;

public class Stock extends Observable {

    /*_______________________Attributs_______________________*/
    private static ArrayList<Integer> list = new ArrayList<Integer>(); //liste des elements du stock
    private static int nbElements; //le nombre d'elements dans le stock
    private static int tailleStock; //la taille du stock

    public Stock(int taille)
    {
    	tailleStock = taille;
    	nbElements = 0;
    }
    /*_______________________Methodes_______________________*/
    /*Retire un element sur le dessus*/
    public void pop(){
        if (!list.isEmpty())
        {
            list.remove(nbElements-1);
            nbElements--;
            notifyObserver();
        }
    }

    /*Remplis la pile avec un element*/
    public void ajouter(int elmt){
        if (list.size() != tailleStock){
            list.add(elmt);
            nbElements++;
    		notifyObserver();
        }
    }
    
    public void clear() {
    	
    	while(nbElements != 0)
    	{
    		this.pop();
    		notifyObserver();
    	}
    	
    }

    /*Retourne le dernier element ajoute*/
    public int afficherElmt(){
        return list.get(nbElements-1);
    }
    
    public int getStock()
    {
    	return nbElements;
    }
    
    public int getTaille()
    {
    	return tailleStock;
    }
}