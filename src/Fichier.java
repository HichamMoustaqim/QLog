import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Fichier implements Observer{

	private String nomFichier;
	private Stock stock;
	private File file;
	private int stockActuel;
	
	public Fichier(String nom, Stock stock)
	{
		this.nomFichier = nom;
		this.stock = stock;
		this.file = new File(this.nomFichier);
		this.stockActuel = this.stock.getStock();
	}
	
    /*_______________________Methodes_______________________*/
	public void setNomFichier(String nom) {
		this.nomFichier = nom;
		this.file = new File(this.nomFichier);
	}
	
	public String getNomFichier() {
		return nomFichier;
	}
	
	public void update() {
		
		int nouveauStock = this.stock.getStock();
		try (BufferedWriter fw = new BufferedWriter(new FileWriter(this.file, true))){
			
			if(nouveauStock > this.stockActuel)
			{
				fw.newLine();
				fw.write("PUSH");
				stockActuel = nouveauStock;
			}
			else if (nouveauStock == this.stockActuel - 1)
			{
				if (nouveauStock == 0)
				{
					fw.newLine();
					fw.write("POP OU CLEAR");
					stockActuel = nouveauStock;
				}
				else
				{
					fw.newLine();
					fw.write("POP");
					stockActuel = nouveauStock;
				}
				
			}
			else if(nouveauStock == 0 && nouveauStock < this.stockActuel - 1)
			{
				fw.newLine();
				fw.write("CLEAR");
				stockActuel = nouveauStock;
			}
			fw.close();
			
		}catch (IOException e) {
			System.out.println("Erreur lors de l'ecriture ! ");
			e.printStackTrace();
		}
	}
}
